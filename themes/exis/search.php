<?php
/**
 * The template for displaying search results pages
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


global $wp_query;

get_header();

?>

<div class="wrapper" id="search-wrapper">

	<div class="container" id="content" tabindex="-1">

		<div class="row">

			<main class="site-main" id="main">
				<header class="page-header">
					<?php get_template_part( 'templates/dermatologists/search-form' ); ?>
				</header><!-- .page-header -->

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php
          //print_r($wp_query);
					$input_name = (isset($_GET['s']))? $_GET['s'] : '';
					$input_field = (isset($_GET['field']))? $_GET['field'] : '';

					if(empty($input_name) && empty($input_field)):
						echo '<p>You must enter a keyword. (Όνομα, πόλη, τκ.)</p>';
					endif;

					while ( have_posts() ) : the_post();

            if(isset($_GET['post_type']))
            {
              $type = $_GET['post_type'];

              if(!empty($type) && $type == 'dermatologists'):
                get_template_part('templates/dermatologists/card');
              endif;
            }
            else
            {
              get_template_part( 'loop-templates/content', 'search' );
            }
					endwhile;
					?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #search-wrapper -->

<?php
get_footer();
