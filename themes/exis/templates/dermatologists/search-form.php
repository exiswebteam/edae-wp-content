<?php
$input_name = (isset($_GET['s']))? $_GET['s'] : '';
$input_field = (isset($_GET['field']))? $_GET['field'] : '';
$semicolon = (!empty($input_name) || !empty($input_field))? ': ': '';
$separator = (!empty($input_name) && !empty($input_field))? '<span> + </span>': '';
?>

<h1 class="page-title"><?= __('Αναζήτηση Δερματολόγου', 'exis');?><?= $semicolon; ?><span class="search-queries"><?= $input_name.$separator.$input_field; ?></span></h1>
<form role="search" method="get" class="dermatologist-search" action="<?php echo home_url( '/' ); ?>">
  <fieldset class="search-wrap">
    <input type="search" class="search-field" name="s" placeholder="<?php echo esc_attr_x( 'Αναζήτηση με βάση το όνομα', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>">
    <input type="text" class="search-field" name="field" placeholder="<?php echo esc_attr_x( 'Αναζήτηση με βάση πόλη ή νομό ή ΤΚ', 'placeholder' ) ?>" value="<?= (isset($_GET['field']))? $_GET['field'] : ''; ?>">
    <input type="hidden" name="post_type" value="dermatologists">
  </fieldset>
  <fieldset class="search-button clear">
    <a href="<?php echo get_post_type_archive_link( 'cpt_dermatologists' ); ?>" class="show-all"><?= __('Όλα τα αποτελέσματα', 'exis'); ?></a>
    <input type="submit" class="search-submit" value="<?= esc_attr_x( 'Αναζήτηση', 'submit button' ) ?>">
  </fieldset>
</form>
