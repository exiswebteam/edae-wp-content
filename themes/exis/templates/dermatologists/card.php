<?php
$address = get_field('address');
$city = get_field('city');
$state = get_field('state');
$postal = get_field('postal_code');
$phone = get_field('phone');
$email = get_field('email');
$website = get_field('website');

$terms = get_the_terms( get_the_ID(), 'dermatologists_taxonomy' );


//Image
$bg_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);

$bg = (!empty($bg_image))? ' style="background-image: url('.$bg_image[0].');"': '';
$placeholder = (empty($bg_image))? ' empty': '';

?>

<article class="dermatologist-card">

  <div class="feat-img bg-cover<?= $placeholder; ?>"<?= $bg; ?>></div>

  <div class="info">
    <h2><?= get_the_title(); ?></h2>

    <?php
    if(!empty($terms)):
      echo '<div class="term mb1">';
      foreach($terms as $term)
      {
        echo '<span class="bold">'.$term->name.'</span>';
      }
      echo '</div>';
    endif; ?>

    <?php if(!empty($address)): ?>
    <span class="field"><?= $address; ?></span>
    <?php endif; ?>

    <?php if(!empty($city)): ?>
    <span class="field">, <?= $city; ?></span>
    <?php endif; ?>

    <?php if(!empty($state)): ?>
    <span class="field"> - <?= $state; ?></span>
    <?php endif; ?>

    <?php if(!empty($postal)): ?>
    <div class="postal"><?= $postal; ?></div>
    <?php endif; ?>

    <?php if(!empty($email)): ?>
    <div class="email">
      <a href="#" class="show-field" data-type="mail:" data-string="<?= $email; ?>"><?= substr($email, 0, 4); ?>*********</a>
    </div>
    <?php endif; ?>

    <?php if(!empty($phone)): ?>
    <div class="phone">
      <a href="#" class="show-field" data-type="tel:" data-string="<?= $phone; ?>"><?= substr($phone, 0, 3); ?>*******</a>
    </div>
    <?php endif; ?>

    <?php if(!empty($website)): ?>
    <div class="website">
      <a href="<?= $website; ?>" target="_blank"><?= $website; ?></a>
    </div>
    <?php endif; ?>

  </div>
</article>
