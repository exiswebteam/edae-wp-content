/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
(function($)
{
  var EXIS_JS = {
  // All pages
  'common': {
    init: function()
    {
      //vars
      var $body = $('body');
      var $window = $(window);

      /* Wait window to fully load and then run functions
      ---------------------------------------------------*/
      $window.load(function()
      {
        $body.addClass('site-loaded');

        /* FN: Visible element on viewport while scrolling
        ----------------------------------------------------*/
        $body.find('.on-viewport').visibleElementFN();
      });//window loaded

      /* FN: Phone and etc.
      ----------------------------------------------------*/
      $body.find('.show-field').displayAllChars();

    },
    finalize: function()
    {
      /* FN: Scroll to fn
      ----------------------------------------------------*/
      $('.scroll-to').scrollToSection();
    }
  },
  'home': {
    init: function()
    {
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var fire;
    var namespace = EXIS_JS;
    funcname = (funcname === undefined) ? 'init' : funcname;
    fire = func !== '';
    fire = fire && namespace[func];
    fire = fire && typeof namespace[func][funcname] === 'function';

    if (fire) {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    // Fire common init JS
    UTIL.fire('common');

    // Fire page-specific init JS, and then finalize JS
    $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
      UTIL.fire(classnm);
      UTIL.fire(classnm, 'finalize');
    });

    // Fire common finalize JS
    UTIL.fire('common', 'finalize');
  }
};

// Load Events
$(document).ready(UTIL.loadEvents);



/**************************************************************************************************/
/************************************   jQuery FUNCTIONS  *****************************************/
/**************************************************************************************************/



/*------------------------------------------------------------------------------------
    Hidden fields
-------------------------------------------------------------------------------------*/
$.fn.displayAllChars = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.not('.clicked').click(function(e)
    {
      if(!$(this).hasClass('clicked'))
      {
        e.preventDefault();

        var _str = ($(this).attr('data-string'))? $(this).attr('data-string') : '';
        var _type = ($(this).attr('data-type'))? $(this).attr('data-type') : '';

        if(_str.length)
        {
          $(this).text(_str);
          $(this).attr('href',_type+_str);
          $(this).addClass('clicked');
        }
        else
        {
          alert('String not found');
        }
      } else {
        //do nothing
      }
    });

  }
};

/*------------------------------------------------------------------------------------
    Scroll to Section
-------------------------------------------------------------------------------------*/
$.fn.scrollToSection = function(_offset)
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $scroll = ($(this).attr('href'))? $(this).attr('href') : $(this).attr('data-id');
      var $targetScroll = $($scroll);

      if($targetScroll.length)
      {
        var _offset_top = ($(this).attr('data-offset'))?parseInt($(this).attr('data-offset')):0;
        var _pos = $targetScroll.offset().top-_offset_top;

        $('html, body').animate({
          scrollTop:_pos
        }, 900);
      }
      else
      {
        alert('Section not found');
      }
    });

  }
};



/*-------------------------------------------------------------------------------
    Visible Element
-------------------------------------------------------------------------------*/
$.fn.visibleElementFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    $(window).scroll(function()
    {
      $target.each(function(i, el)
      {
        var _el = $(el);

        if (_el.onViewportFN(true))
        {
          _el.addClass("element-visible");
        }
      });
    });
  }
};


/*-------------------------------------------------------------------------------
    On Viewport Element - Visible
-------------------------------------------------------------------------------*/
$.fn.onViewportFN = function(partial)
{
  var $t            = $(this),
      $w            = $(window),
      viewTop       = $w.scrollTop(),
      viewBottom    = viewTop + $w.height(),
      _top          = $t.offset().top+90,
      _bottom       = _top + $t.height(),
      compareTop    = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};


})(jQuery); /******** Fully reference jQuery after this point. *************/
