<?php
class DERMATOLOGISTS_custom_post_type
{

  /* Construct
  ----------------------------------------------------------------------------------------*/
	public function __construct( )
	{
    //1. Post Type
    add_action( 'init', array( $this, 'register_dermatologists_cpt' ) );
    //2. Custom taxonomy
    add_action( 'init', array( $this, 'register_dermatologists_taxonomies' ) );
		//3. Search
		add_filter( 'pre_get_posts', array( $this, 'search_filter' ) );
	}


  function register_dermatologists_cpt()
  {
    $singular = 'Dermatologist';
    $singular_lowercase = 'dermatologist';
    $plural = 'Dermatologists';
    $slug = 'dermatologists';
    $post_type = 'cpt_dermatologists';
    $desc = 'ΕΔΑΕ Δερματολόγοι';
    $supports = array('title','editor','custom-fields','revisions','thumbnail','page-attributes');

	  //Labels
    $labels = array(
    'name' => _x( $plural, 'post type general name'),
    'singular_name' => _x( $singular, 'post type singular name'),
    'add_new' => _x('Add New', $singular_lowercase ),
    'add_new_item' => __('Add New '. $singular_lowercase),
    'edit_item' => __('Edit '. $singular_lowercase ),
    'new_item' => __('New '. $singular_lowercase ),
    'view_item' => __('View '. $singular_lowercase),
    'search_items' => __('Search '. $plural),
    'not_found' =>  __('No '. $singular .' found'),
    'not_found_in_trash' => __('No '. $singular .' found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => $plural
    );

    //Args
    $args = array(
    'labels' => $labels,
    'description' => $desc,
    'public' => true,
    'menu_position' => 20,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_rest' => false,
    'query_var' => true,
    'menu_icon' => 'dashicons-businessperson',
    'rewrite' => array('slug'=> $slug ),
    'capability_type' => 'post',
    'has_archive' => true,
    'exclude_from_search' => false,
    'hierarchical' => false,
    'supports' => $supports
    );

    register_post_type( $post_type, $args );
  }


  function register_dermatologists_taxonomies()
  {

    //Taxonomies Properties
      $taxonomies = array(
        array(
          'slug'         => 'dermatologists_taxonomy',
          'single_name'  => 'Κατηγορία',
          'plural_name'  => 'Κατηγορία',
          'post_type'    => array('cpt_dermatologists'),
          'hierarchical' => true,
          'rewrite'      => array( 'slug' => 'dermatologists-category', 'with_front' => false),
        ),
      );
      //Register
      foreach( $taxonomies as $taxonomy )
      {
        $labels = array(
          'name' => $taxonomy['plural_name'],
          'singular_name' => $taxonomy['single_name'],
          'search_items' =>  'Search ' . $taxonomy['plural_name'],
          'all_items' => 'All ' . $taxonomy['plural_name'],
          'parent_item' => 'Parent ' . $taxonomy['single_name'],
          'parent_item_colon' => 'Parent ' . $taxonomy['single_name'] . ':',
          'edit_item' => 'Edit ' . $taxonomy['single_name'],
          'update_item' => 'Update ' . $taxonomy['single_name'],
          'add_new_item' => 'Add New ' . $taxonomy['single_name'],
          'new_item_name' => 'New ' . $taxonomy['single_name'] . ' Name',
          'menu_name' => $taxonomy['plural_name']
        );
        //For no errors
        $rewrite = isset( $taxonomy['rewrite'] ) ? $taxonomy['rewrite'] : array( 'slug' => $taxonomy['slug'] );
        $hierarchical = isset( $taxonomy['hierarchical'] ) ? $taxonomy['hierarchical'] : true;
        //Register
        register_taxonomy( $taxonomy['slug'], $taxonomy['post_type'], array(
          'hierarchical' => $hierarchical,
          'labels' => $labels,
          'show_ui' => true,
          'show_admin_column' => true,
          'query_var' => true,
          'rewrite' => $rewrite,
          'show_in_rest' => false,
          'show_in_nav_menus' => true,
        ));
     }
  }// END taxonomy


	function search_filter($query)
	{
		// Extend search for document post type
    $post_type = 'dermatologists';

    // Custom fields to search for
    $custom_fields = array(
				'_city',
				'_state',
				'_postal_code'
    );

		if(isset($_GET['post_type']))
		{
			$search_type = $_GET['post_type'];

			if($search_type == 'dermatologists')
			{
				//set query
				$query->set( 'post_type', array('cpt_dermatologists') );

				if(isset($_GET['field']))
				{
					$search_field = (isset($_GET['field']))? $_GET['field'] : '';

					$meta_query = array( 'relation' => 'OR' );

	        foreach( $custom_fields as $custom_field )
					{
            array_push( $meta_query, array(
                'meta_key' => $custom_field,
                'value' => $search_field,
                'compare' => 'LIKE'
            ));
	        }

					//set query
					$query->set( 'meta_query', $meta_query );
				}
			}
    }
	}



}//END Class

new DERMATOLOGISTS_custom_post_type();
